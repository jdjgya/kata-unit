package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"golang.org/x/net/websocket"
)

func DeliverMsg(ws *websocket.Conn) {
	var err error

	// Setup msg
	msg := `{ "Msg":"this is mock websocket server" }`

	// Send msg to client when handshake is done.
	for {
		// Send msg to websocket client
		time.Sleep(3 * time.Second)
		if err = websocket.Message.Send(ws, msg); err != nil {
			fmt.Println("[Error] Unable to send msg")
		} else {
			fmt.Println("[Info] Success to send msg")
		}
	}
}

func main() {
	// Create the websocet connection
	http.Handle("/", websocket.Handler(DeliverMsg))
	if err := http.ListenAndServeTLS("127.0.0.1:9999", "test.crt", "test.key", nil); err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}
