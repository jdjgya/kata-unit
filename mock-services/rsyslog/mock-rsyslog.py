import logging
import logging.config
from socket import *

# Set the socket parameters
bind_ip = '127.0.0.1'
bind_port = 514
buffer_size = 1024
bind_socket = (bind_ip, bind_port)

# Create socket and bind to address
rsyslogService = socket(AF_INET,SOCK_DGRAM)
rsyslogService.bind(bind_socket)
print "[*] Mock Rsyslog Server is running"
print "[*] Listening on %s:%d" % (bind_ip, bind_port)

# Receive messages from rsyslog client
while True:
    message, addr = rsyslogService.recvfrom(buffer_size)

    # Show up the extracted messages
    print "[*] Received messages:-------------------------------------------------------------"
    print message
    print "-----------------------------------------------------------------------------------"

    logging.debug('Receive the request from testing client')

# Close socket
rsyslogService.close()
