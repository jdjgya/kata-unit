import socket
import threading
import ssl
import time
import random
import uuid
import re
import urllib2
import json
import logging
import logging.config

bind_ip = '127.0.0.1'
bind_port = 8888
buffer_size = 2048
timeout = 10

def handle_client(client_socket, streamId_connection):
    try:
        packetNumber = 0
        request = ""
        while packetNumber >= 0:
            # Receive messages from client, and save messages to "request"
            client_socket.settimeout(timeout)
            datbuf = client_socket.recv(buffer_size)
            if not datbuf:
                break
            request += datbuf

            # Show up the extracted messages
            print "[*] Received messages:-------------------------------------------------------------"
            print datbuf
            print "-----------------------------------------------------------------------------------"

            packetNumber = packetNumber + 1
    except:
        # Close any connections if they don't recived any messages from client 
        print '[*] No more data come from client anymore, close connection'+'\n'
    finally:
        # Return HTTP 200 to record module
        method = request.split("\r\n")[0].split()[0]
        print "METHOD: %s" % method
        if method == 'POST':
            client_socket.sendall("HTTP/1.0 200 OK\r\n")
            client_socket.sendall("Content-Type: application/json\r\n\r\n")

        # Return HTTP 400 and mock-error message to client when the method/API is not acceptable
        else:
            client_socket.sendall("HTTP/1.0 400 OK\r\n")
            client_socket.sendall("Content-Type: text/html\r\n\r\n")
            client_socket.sendall('{"Error":"Bad request from client"}')

        client_socket.close()

if (__name__ == '__main__'):
    # Create a socket for receiving the out-side request
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Force re-use opened socket
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind((bind_ip, bind_port))
    server.listen(5)
    streamId_connection = 0
    print "[*] Mock Https server is running now."
    print "[*] Listening on %s:%d" % (bind_ip, bind_port)

    # Use SSL protocol to encapsulate the http connection
    while True:
        client, addr = server.accept()
        streamId_connection = streamId_connection+ 1
        ssl_client=ssl.wrap_socket(client, server_side=True, \
                                    certfile="./testCA/server.crt", \
                                    keyfile="./testCA/server.key", \
                                    ssl_version=ssl.PROTOCOL_TLSv1)
        print "[*] Accepted connection from: %s:%d" %(addr[0], addr[1])
        print "[*] StreamId for current connection: %s:" %(streamId_connection)

        # Listen on the specificed port and use handle_client to catch incoming request
        client_handler = threading.Thread(target=handle_client, args=(ssl_client, streamId_connection))
        client_handler.start()
        client.close()
