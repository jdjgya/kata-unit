package main

import (
	"dns/cds"
	"encoding/json"
	"log"
	"runtime"
	"time"

	"github.com/gocql/gocql"
	nats "github.com/nats-io/go-nats"
)

//
// The function for getting the Malware Type
//
func GetMalwareType(dgaFamily string) string {

	// Define the mapping for DGA family <---> Malware type
	dgaToMalware := make(map[string]string)
	dgaToMalware["banjori"] = "Banking Trojan"
	dgaToMalware["benign"] = "benign"
	dgaToMalware["chinad"] = "Trojan Horse"
	dgaToMalware["corebot"] = "Banking Trojan"
	dgaToMalware["dircrypt"] = "Ransomware"
	dgaToMalware["fobber"] = "Banking Trojan"
	dgaToMalware["gozi"] = "Banking Trojan"
	dgaToMalware["kraken"] = "Botnet"
	dgaToMalware["locky"] = "Ransomware"
	dgaToMalware["murofet"] = "Banking Trojan"
	dgaToMalware["necurs"] = "Botnet"
	dgaToMalware["newgoz"] = "Banking Trojan"
	dgaToMalware["nymaim"] = "Trojan Horse"
	dgaToMalware["padcrypt"] = "Ransomware"
	dgaToMalware["proslikefan"] = "Worm"
	dgaToMalware["pykspa"] = "Worm"
	dgaToMalware["qadars"] = "Banking Trojan"
	dgaToMalware["qakbot"] = "Banking Trojan"
	dgaToMalware["ramdo"] = "Trojan Horse"
	dgaToMalware["ramnit"] = "Banking Trojan"
	dgaToMalware["ranbyus"] = "Banking Trojan"
	dgaToMalware["shiotob"] = "Banking Trojan"
	dgaToMalware["simda"] = "Trojan Horse"
	dgaToMalware["sisron"] = "Trojan Horse"
	dgaToMalware["suppobox"] = "Trojan Horse"
	dgaToMalware["symmi"] = "Trojan Horse"
	dgaToMalware["tempedreve"] = "Worm"
	dgaToMalware["tinba"] = "Banking Trojan"
	dgaToMalware["vawtrak"] = "Banking Trojan"

	// DGA and Malware type mapping
	for familyName := range dgaToMalware {
		if familyName == dgaFamily {
			return dgaToMalware[familyName]
		}
	}

	// Return UnknownMalware if can't map to any malware types
	return "UnknownMalware"
}

//
// The function for getting analytic result from broker
//
func GetResultFromBroker(cdsChannel chan cds.DomainsCDS, subject string) {
	// Connect to NATS; Defer NATS close
	for {
		// Init NATS connection
		natsConnection, connErr := nats.Connect("nats://localhost:4222")
		log.Println("Start to get messages from subject: ", subject)

		// Check NATS health status
		if connErr != nil {
			log.Println("NATS server is not available right now, try to reconnect...")
			time.Sleep(5 * time.Second)
			continue
		}

		// Subscribe to subject and pass cds to channel
		var analyticResults cds.DomainsCDS
		natsConnection.Subscribe(subject, func(msg *nats.Msg) {
			json.Unmarshal(msg.Data, &analyticResults)
			cdsChannel <- analyticResults
			log.Println("Sent analytic results to channel")
		})

		// Handle NATS connection status
		for {
			// Check NATS health status
			if natsConnection.Status() != 1 {
				log.Println("NATS server connection refused")
				natsConnection.Close()
				time.Sleep(5 * time.Second)
				break
			}
			time.Sleep(10 * time.Second)
		}
	}
}

//
// The function for sending the analytic results to database
//
func UpdateResultToDB(cdsChannel chan cds.DomainsCDS) {
	// Connect to ScyllaDB; Defer close
	for {
		// Init ScyllaDB client by CQL
		cluster := gocql.NewCluster("127.0.0.1")
		cluster.Port = 9042
		session, connErr := cluster.CreateSession()
		defer session.Close()

		// Check ScyllaDB health status before creating keyspace and table
		if connErr != nil {
			// Re-Init ScyllaDB connect if got connection errors
			log.Println("ScyllaDB is unvailable right now, trying to reconnect...")
			time.Sleep(5 * time.Second)
			continue
		}

		// Create keyspace if not exists
		createKeyspace := session.Query(`CREATE KEYSPACE IF NOT EXISTS test WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 }`)
		createKeyspace.Exec()
		createKeyspace.Release()

		// Create the schema of DGA table
		const analyticSchema = `
			CREATE TABLE IF NOT EXISTS test.dga_family (
				dgafamily   text,
				malwaretype text,
				count       counter,
				PRIMARY KEY(dgafamily, malwaretype)
		)`

		// Create DGA-Family table
		createTable := session.Query(analyticSchema)
		createTable.Exec()
		createTable.Release()

		// Handle ScyllaDB connection and upadate analytic result
		for {
			// Check ScyllaDB health status
			if connErr != nil {
				log.Println("ScyllaDB is unvailable right now, trying to reconnect...")
				time.Sleep(5 * time.Second)
				break
			}

			// Get CDS from channel
			results, ok := <-cdsChannel
			if ok == false {
				log.Println("Event channel has been closed")
				continue
			}

			//
			// ** Prevent the execution of the batch if greater than the limit **
			// ** Currently batches have a limit of 65536 queries. **
			// ** https://datastax-oss.atlassian.net/browse/JAVA-229 **
			//
			// Creat Batch update task for ScyllaDB
			batchSize := 50000
			var resultBatches [][]cds.Domain
			log.Println("origin size", len(results.DomainsCDS))
			for batchSize < len(results.DomainsCDS) {
				results.DomainsCDS, resultBatches = results.DomainsCDS[batchSize:], append(resultBatches, results.DomainsCDS[0:batchSize:batchSize])
			}
			resultBatches = append(resultBatches, results.DomainsCDS)

			// Update DGA table by each batch tasks
			for i := range resultBatches {
				// Create a container for storing batch queries
				batch := session.NewBatch(gocql.UnloggedBatch)
				for b := range resultBatches[i] {
					dgaFamily := resultBatches[i][b].Analyticresult[0]
					malwareType := GetMalwareType(dgaFamily)
					batch.Query(`UPDATE test.dga_family SET count = count + 1 WHERE dgafamily=? AND malwaretype=?`, dgaFamily, malwareType)
				}
				// Do Batch update task for ScyllaDB
				if err := session.ExecuteBatch(batch); err != nil {
					log.Println("Unable to update the DGA family due to ", err)
					break
				} else {
					log.Println("Upadated Domains: ", len(results.DomainsCDS))
				}
			}
			time.Sleep(10 * time.Second)
		}
	}
}

//
// The function for processing analyitc results
//
func DoWork() {
	// Set up a Channel for inter-thread communication
	cdsChannel := make(chan cds.DomainsCDS, 400000)

	// Bring up threads to capture event and parsing events
	go GetResultFromBroker(cdsChannel, "record")
	go UpdateResultToDB(cdsChannel)
	runtime.Goexit()
}

func main() {
	DoWork()
}
