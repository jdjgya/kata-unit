package main

import (
	"crypto/tls"
	"dns/cds"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"runtime"
	"strings"
	"time"

	nats "github.com/nats-io/go-nats"
)

//
// The function for getting ariel events from qradar
//
func GetArielEvents(eventChannel chan cds.ArielEvents) {
	// Connect to ArelDB and pass the ariel events to channel
	for {
		// Set Ariel connection info
		url := "https://172.17.0.2/api/ariel/searches"
		user := "admin"
		password := "RealSecure!"
		arielSQL := "SELECT LOGSOURCETYPENAME(devicetype) as log_source_type, sourceip, destinationip, sourcev6, destinationv6, sourceport, destinationport, url AS fqdn, dns_request_type, XFORCE_URL_CATEGORY(fqdn) AS categories FROM events WHERE fqdn IS NOT NULL AND log_source_type != 'IBM DNS Analyzer'"

		// Encode query stmt to json
		aqlQuery := map[string]string{"query_expression": arielSQL}
		jsonAQL, _ := json.Marshal(aqlQuery)

		// Create POST connection
		req, _ := http.NewRequest("POST", url, strings.NewReader(string(jsonAQL)))
		req.SetBasicAuth(user, password)
		req.Close = true

		// Encode Ariel query to HTTP connection
		query := req.URL.Query()
		query.Add("query_expression", arielSQL)
		req.URL.RawQuery = query.Encode()

		// Do Ariel query in insecure connection
		insecure := &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}
		client := &http.Client{Transport: insecure}
		resp, err := client.Do(req)

		// Reconnect to Ariel if it's not avilable
		if err != nil {
			log.Println("Ariel-DB is not available right now")
			time.Sleep(5 * time.Second)
			continue
		}

		// Pass body msg to struct
		body, _ := ioutil.ReadAll(resp.Body)
		var arielEvents cds.ArielEvents
		json.Unmarshal(body, &arielEvents)
		resp.Body.Close()
		log.Println("Got Ariel Event: ", len(arielEvents.Events))

		// Pass the ariel events into channel
		eventChannel <- arielEvents
		time.Sleep(10 * time.Second)
	}
}

//
// The function for parsing and filtering the domain info
//
func ParseDNSInfo(eventChannel chan cds.ArielEvents) {
	// Convert Ariel events to CDS and pass CDS to channel
	for {
		// Receive captured ariel events from channel
		arielEvents, ok := <-eventChannel
		if ok == false {
			log.Println("Event channel has been closed")
			continue
		}

		// Filter out known domain names i.e. the Category size is not 0
		for i := len(arielEvents.Events) - 1; i >= 0; i-- {
			if len(arielEvents.Events[i].Categories) != 0 {
				arielEvents.Events = append(arielEvents.Events[:i], arielEvents.Events[i+1:]...)
			}
		}
		log.Println("Domains without XFE Categories: ", len(arielEvents.Events))

		// Pass ariel info to CDS
		domain := make([]cds.Domain, len(arielEvents.Events))
		for i := range arielEvents.Events {
			domain[i].Fqdn = arielEvents.Events[i].Fqdn
			domain[i].Sourceip = arielEvents.Events[i].Sourceip
			domain[i].Destinationip = arielEvents.Events[i].Destinationip
			domain[i].Dnsrequesttype = arielEvents.Events[i].Dnsrequesttype
			// ** Following line is used to do test without analytic modules **
			// domain[i].Analyticresult = []string{"Benign"}
		}

		// Pass CDS set to NATS
		sendErr := SendCDSToBroker(&cds.DomainsCDS{domain}, "nod")
		if sendErr != nil {
			log.Println("Unable to send CDS to NATS")
		} else {
			log.Println("Domains parsed: ", len(domain))
		}
	}
}

//
// The function for sending CDS to NATS
//
func SendCDSToBroker(cdsList *cds.DomainsCDS, subject string) error {
	// Connect to NATS; Defer NATS close
	// Init NATS connection
	natsConnection, connErr := nats.Connect("nats://localhost:4222")
	defer natsConnection.Close()

	// Handle NATS connection and send CDS
	if connErr != nil {
		log.Println("NATS server is unavailable right now")
		natsConnection.Close()
		return connErr
	}

	// Publish message into subject
	jsonCDS, _ := json.Marshal(cdsList)
	pubErr := natsConnection.Publish(subject, []byte(string(jsonCDS)))
	if pubErr != nil {
		log.Println("Unable to Pub message due to ", pubErr)
		natsConnection.Close()
		return pubErr
	} else {
		log.Println("Send CDS to NATS")
	}

	return nil
}

//
// The function for processing raw dns data
//
func DoWork() {
	// Set up a Channel for inter-thread communication
	eventChannel := make(chan cds.ArielEvents, 400000)

	// Bring up threads to capture events, parse events and send CDS
	go GetArielEvents(eventChannel)
	go ParseDNSInfo(eventChannel)
	runtime.Goexit()
}

func main() {
	DoWork()
}
