package main

import "fmt"

type Domain struct {
	Fqdn           string `json:"fqdn"`
	Domainname     string `json:"Domainname"`
	Analyticresult string `json:"analyticresult"`
}

func Aggregate(t *map[string]int, msg *Domain) {
	key := msg.Domainname + msg.Analyticresult
	if _, found := (*t)[key]; found {
		(*t)[key]++
	} else {
		(*t)[key] = 1
	}
}

func main() {
	var mockMsg1 = Domain{Fqdn: "api.google.com", Domainname: "google.com", Analyticresult: "Benign"}
	var mockMsg2 = Domain{Fqdn: "api.google.com", Domainname: "google.com", Analyticresult: "Benign"}

	table := make(map[string]int)

	Aggregate(&table, &mockMsg1)
	Aggregate(&table, &mockMsg2)

	for key, value := range table {
		fmt.Println(key)
		fmt.Println(value)
	}
}
