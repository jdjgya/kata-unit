package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	// Generate unsorted slice
	// Assign random value to each element
	var unsortList []int = make([]int, 10)
	rand.Seed(time.Now().UnixNano())
	for index := range unsortList {
		unsortList[index] = rand.Intn(100)
	}
	fmt.Println("Unsorted list: ", unsortList)

	// First loop is used to check whether the slice comleted sorting?
	for true {
		var movedCounter int = 0

		// Second loop is used to switch the elements in the slice
		for currentIndex := range unsortList[0:9] {
			if unsortList[currentIndex] != unsortList[currentIndex+1] {
				if unsortList[currentIndex] > unsortList[currentIndex+1] {
					var moveToNextIndex int = unsortList[currentIndex]
					unsortList[currentIndex] = unsortList[currentIndex+1]
					unsortList[currentIndex+1] = moveToNextIndex

					// Increase counter if there's a element has been changed during the iteration
					movedCounter++
				}
			}
		}
		fmt.Println("Changed list", unsortList)

		// Leave loop if counter is keep 0 after go through the whole slice
		if movedCounter == 0 {
			break
		}
	}
}
