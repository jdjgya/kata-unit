package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	// Generate unsorted slice
	// Assign random value to each element
	var unsortList []int = make([]int, 10)
	rand.Seed(time.Now().UnixNano())
	for index := range unsortList {
		unsortList[index] = rand.Intn(100)
	}
	fmt.Println("Unsorted list: ", unsortList)

	// Setup lower and start index for every iteration
	var lowerIndex int = 0
	var startIndex int = 0
	var tmp int

	// First loop is used to check whether sorting is completed
	for true {
		if startIndex < len(unsortList) {
			// Second loop is used to find the lower in each iteration
			for currentIndex := range unsortList[startIndex:len(unsortList)] {
				if unsortList[currentIndex+startIndex] != unsortList[lowerIndex] {
					if unsortList[currentIndex+startIndex] < unsortList[lowerIndex] {
						lowerIndex = currentIndex + startIndex
					}
				}
			}

			// Move the lower to start index of iteration
			tmp = unsortList[lowerIndex]
			unsortList[lowerIndex] = unsortList[startIndex]
			unsortList[startIndex] = tmp
			fmt.Println(unsortList)

			// Change the start index for next iteration
			startIndex = startIndex + 1
			lowerIndex = startIndex
		} else {
			break
		}
	}
}
