package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	// Generate unsorted slice
	// Assign random value to each element
	var unsortList []int = make([]int, 10)
	rand.Seed(time.Now().UnixNano())
	for index := range unsortList {
		unsortList[index] = rand.Intn(100)
	}
	fmt.Println("Unsorted list: ", unsortList)

	var i, j, tmp int
	for i = 1; i < len(unsortList); i++ {
		tmp = unsortList[i]
		for j = i; j > 0 && tmp < unsortList[j-1]; j-- {
			unsortList[j] = unsortList[j-1]
		}
		unsortList[j] = tmp
		fmt.Println(unsortList)
	}
}
