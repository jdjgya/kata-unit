package main

import "stateless/internal/controller"

func main() {
	// Init service
	controller.LoadConfFile()
	c := controller.NewController()

	// Start service
	c.Start()
	c.Wait()
}
