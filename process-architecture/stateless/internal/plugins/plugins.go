package plugins

import (
	"stateless/internal/plugins/input"
	"stateless/internal/plugins/output"
	"stateless/internal/plugins/process"
)

// This is place to define the shared space of plugins regarding:
//
// 1). Input plugins
// 2). Process plugins
// 3). Output plugins
var (
	Input   = make(map[string]input.Input)
	Process = make(map[string]process.Process)
	Output  = make(map[string]output.Output)
)
