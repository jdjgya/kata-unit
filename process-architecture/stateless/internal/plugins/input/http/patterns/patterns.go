package patterns

var (
	Plugins = make(map[string]Handler)
)

// Input interface is used to:
//
// 1). Define the behavior of Input plugins
// 2). Plug() is used to link/set the control to plugins
// 3). DoInput() is used to handle the incoming data
// 4). UnPlug() is used to unlink/unset the control from plugins
type Handler interface {
	RegisterHandlers()
}
