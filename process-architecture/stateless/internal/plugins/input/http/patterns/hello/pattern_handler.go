package hello

import (
	"fmt"
	"net/http"
	"stateless/internal/plugins/input/http/patterns"
)

func init() {
	patterns.Plugins["hello"] = &Hello{}
}

type Hello struct{}

func hello(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "hello\n")
}

func (a *Hello) RegisterHandlers() {
	http.HandleFunc("/hello", hello)
}
