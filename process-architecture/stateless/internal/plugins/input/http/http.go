package http

import (
	"context"
	"net/http"
	"stateless/internal/plugins/input"
	"stateless/internal/plugins/input/http/patterns"
	_ "stateless/internal/plugins/input/http/patterns/hello"
	"sync"

	log "github.com/sirupsen/logrus"
)

// init() is used to register the functions/plugins into shared space of plugin
func init() {
	wg := &sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())
	input.Plugins["http"] = &Http{ctx: ctx, cancel: cancel, wg: wg}
	patterns.Plugins["hello"].RegisterHandlers()
}

// Dummy struct is used to define how we control the plugin
type Http struct {
	instance *http.Server
	wg       *sync.WaitGroup
	ctx      context.Context
	cancel   context.CancelFunc
}

// DoInput() is a simple example which used to show:
//
// 1). The implementation for handling the incoming data
// 2). The way we use to cancel the job
func (h *Http) DoInput(chnI2P chan interface{}, metrics *map[string]map[string]int32, records *map[string][]string) {
	h.wg.Add(1)
	defer h.wg.Done()

	h.instance = &http.Server{Addr: ":8080"}
	log.Infof("after new instance")

	h.instance.ListenAndServe()
	log.Infof("after httpServer")
}

// UnPlug() is used to unset the control with main process
func (h *Http) Stop() {
	log.Info("Worker-Input: http cancel has been called~~~~")
	// h.cancel()
	h.instance.Shutdown(nil)
	h.wg.Wait()
	log.Info("Worker-Input: unplug input http plugin")

	h.ctx, h.cancel = context.WithCancel(context.Background())
}
