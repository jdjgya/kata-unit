package dummy

import (
	"context"
	"stateless/internal/plugins/input"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
)

// init() is used to register the functions/plugins into shared space of plugin
func init() {
	wg := &sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())
	input.Plugins["dummy"] = &Dummy{ctx: ctx, cancel: cancel, wg: wg}
}

// Dummy struct is used to define how we control the plugin
type Dummy struct {
	wg     *sync.WaitGroup
	ctx    context.Context
	cancel context.CancelFunc
}

// DoInput() is a simple example which used to show:
//
// 1). The implementation for handling the incoming data
// 2). The way we use to cancel the job
func (d *Dummy) DoInput(chnI2P chan interface{}, metrics *map[string]map[string]int32, records *map[string][]string) {
	d.wg.Add(1)
	defer d.wg.Done()

	for {
		select {
		case <-d.ctx.Done():
			log.Infof("Worker-Input: Got cancel signal. Stop Worker-Input")
			return
		default:
			// Handle incoming data
			dummyStr := "Hi, this is dummy input"
			chnI2P <- dummyStr
			log.Infof("Worker-Input: input record - '%v'", dummyStr)

			// Record statistical metrics
			(*metrics)["input"]["dummy_in"] = (*metrics)["input"]["dummy_in"] + 1
			(*metrics)["input"]["dummy_process"] = (*metrics)["input"]["dummy_process"] + 1
			(*metrics)["input"]["dummy_out"] = (*metrics)["input"]["dummy_out"] + 1
			(*records)["dummy_input"] = append((*records)["dummy_input"], dummyStr)

			time.Sleep(1 * time.Second)
		}
	}
}

// UnPlug() is used to unset the control with main process
func (d *Dummy) Stop() {
	d.cancel()
	d.wg.Wait()
	log.Info("Worker-Input: unplug input plugin")

	d.ctx, d.cancel = context.WithCancel(context.Background())
}
