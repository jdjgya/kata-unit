package output

import (
	"context"
	"sync"
)

var (
	Plugins = make(map[string]Output)
)

// Output interface is used to:
//
// 1). Define the behavior of Output plugins
// 2). Plug() is used to link/set the control to plugins
// 3). DoOutput() is used to handle the received data which sent by Process plugin
// 4). UnPlug() is used to unlink/unset the control from plugins
type Output interface {
	Plug(*context.Context, *sync.WaitGroup)
	DoOutput(chan interface{}, *(map[string]map[string]int32), *(map[string][]string))
	UnPlug()
}
