package dummy

import (
	"context"
	"stateless/internal/plugins/output"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
)

// init() is used to register the functions/plugins into shared space of plugin
func init() {
	output.Plugins["dummy"] = &Dummy{}
}

// Dummy struct is used to define how we control the plugin
type Dummy struct {
	ctx       *context.Context
	waitGroup *sync.WaitGroup
}

// Plug() is used to set the control with main process
func (d *Dummy) Plug(ctx *context.Context, wg *sync.WaitGroup) {
	d.ctx = ctx
	d.waitGroup = wg

	d.waitGroup.Add(1)
	log.Info("Worker-Output: plug output plugin")
}

// DoOutput() is a simple example which used to show:
//
// 1). The implementation for handling the data from Process plugin
// 2). The way we use to cancel the job
func (d *Dummy) DoOutput(chnP2O chan interface{}, metrics *(map[string]map[string]int32), records *(map[string][]string)) {
	defer d.UnPlug()

	for {
		select {
		case <-(*d.ctx).Done():
			log.Infof("Worker-Output: Got cancel signal. Stop Worker-Output")
			return
		default:
			// Handle data from Process plugin
			dummyStr := <-chnP2O
			log.Infof("Worker-Output: output record - '%v'", dummyStr)

			// Record statistical metrics
			(*metrics)["output"]["dummy_in"] = (*metrics)["output"]["dummy_in"] + 1
			(*metrics)["output"]["dummy_output"] = (*metrics)["output"]["dummy_process"] + 1
			(*metrics)["output"]["dummy_out"] = (*metrics)["output"]["dummy_out"] + 1
			(*records)["dummy_output"] = append((*records)["dummy_output"], dummyStr.(string))

			time.Sleep(1 * time.Second)
		}
	}
}

// UnPlug() is used to unset the control with main process
func (d *Dummy) UnPlug() {
	log.Info("Worker-Output: unplug output plugin")
	d.waitGroup.Done()
}
