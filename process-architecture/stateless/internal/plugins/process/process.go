package process

import (
	"context"
	"sync"
)

var (
	Plugins = make(map[string]Process)
)

// Process interface is used to:
//
// 1). Define the behavior of Process plugins
// 2). Plug() is used to link/set the control to plugins
// 3). DoProcess() is used to handle the received data which sent by Input plugin
// 4). UnPlug() is used to unlink/unset the control from plugins
type Process interface {
	Plug(*context.Context, *sync.WaitGroup)
	DoProcess(chan interface{}, chan interface{}, *(map[string]map[string]int32), *(map[string][]string))
	UnPlug()
}
