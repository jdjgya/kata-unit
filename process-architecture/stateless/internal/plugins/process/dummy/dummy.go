package dummy

import (
	"context"
	"stateless/internal/plugins/process"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
)

// init() is used to register the functions/plugins into shared space of plugin
func init() {
	process.Plugins["dummy"] = &Dummy{}
}

// Dummy struct is used to define how we control the plugin
type Dummy struct {
	ctx       *context.Context
	waitGroup *sync.WaitGroup
}

// Plug() is used to set the control with main process
func (d *Dummy) Plug(ctx *context.Context, wg *sync.WaitGroup) {
	d.ctx = ctx
	d.waitGroup = wg

	d.waitGroup.Add(1)
	log.Info("Worker-Process: plug process plugin")
}

// DoProcess() is a simple example which used to show:
//
// 1). The implementation for handling the data from Input plugins and send data to Output plugin
// 2). The way we use to cancel the job
func (d *Dummy) DoProcess(chnI2P chan interface{}, chnP2O chan interface{}, metrics *(map[string]map[string]int32), records *(map[string][]string)) {
	defer d.UnPlug()

	for {
		select {
		case <-(*d.ctx).Done():
			log.Infof("Worker-Process: Got cancel signal. Stop Worker-Process")
			return
		default:
			// Handle the data from Input plugin
			dummyStr := <-chnI2P

			// Send the data to Outpu plugin
			chnP2O <- dummyStr
			log.Infof("Worker-Process: processed record - '%v'", dummyStr)

			// Record statistical metrics
			(*metrics)["process"]["dummy_in"] = (*metrics)["process"]["dummy_in"] + 1
			(*metrics)["process"]["dummy_process"] = (*metrics)["process"]["dummy_process"] + 1
			(*metrics)["process"]["dummy_out"] = (*metrics)["process"]["dummy_out"] + 1
			(*records)["dummy_process"] = append((*records)["dummy_process"], dummyStr.(string))

			time.Sleep(1 * time.Second)
		}
	}
}

// UnPlug() is used to unset the control with main process
func (d *Dummy) UnPlug() {
	log.Info("Worker-Process: unplug process plugin")
	d.waitGroup.Done()
}
