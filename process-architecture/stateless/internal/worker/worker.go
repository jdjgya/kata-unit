package worker

import (
	"stateless/internal/plugins/input"
	_ "stateless/internal/register_plugin"

	config "github.com/spf13/viper"
)

// worker struct is used to
//
// 1. Handle the incoming data in the data plane
// 2. Record statistical metrics
// 3. Record incoming data
type Worker struct {
	input   string
	process string
	output  string
	Metrics *map[string]map[string]int32
	Records *map[string][]string
}

// NewWorker() is used to
//
// 1. instantiate the worker
// 2. init the necessary vars
// 3. return instance's pointer to caller(generally, the caller will be controller)
func NewWorker(input, process, output string) *Worker {
	metrics := make(map[string]map[string]int32)
	metrics["input"] = make(map[string]int32)
	metrics["process"] = make(map[string]int32)
	metrics["output"] = make(map[string]int32)

	records := make(map[string][]string)

	w := &Worker{input: input, process: process, output: output, Metrics: &metrics, Records: &records}
	return w
}

// DoWork() is used to
//
// 1. instantiate the worker
// 2. init the necessary vars
// 3. bring up worker goroutines to handle the data plane stuff
func (w *Worker) DoWork() {
	chnBuffer := config.GetInt32("channel-buffer")

	// Define the communication channels
	chnI2P := make(chan interface{}, chnBuffer)
	// chnP2O := make(chan interface{}, chnBuffer)

	// Activate the specified plugins for handling incoming data
	go input.Plugins[w.input].DoInput(chnI2P, w.Metrics, w.Records)
	// go process.Plugins[w.process].DoProcess(chnI2P, chnP2O, w.Metrics, w.Records)
	// go output.Plugins[w.output].DoOutput(chnP2O, w.Metrics, w.Records)
}
