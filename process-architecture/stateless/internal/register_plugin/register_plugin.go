package register_plugin

// This is place to register the plugins which was implemented under the:
//
// 1). stateless/internal/plugins/input/
// 2). stateless/internal/plugins/process/
// 3). stateless/internal/plugins/output/
import (
	_ "stateless/internal/plugins/input/dummy"
	_ "stateless/internal/plugins/input/http"
	_ "stateless/internal/plugins/output/dummy"
	_ "stateless/internal/plugins/process/dummy"
)
