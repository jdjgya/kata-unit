package controller

import (
	"bytes"
	"context"
	"flag"
	"io/ioutil"
	"os"
	"os/signal"
	"stateless/internal/helper/cron"
	"stateless/internal/helper/dummy"
	"stateless/internal/plugins/input"
	"stateless/internal/worker"
	"sync"
	"syscall"
	"time"

	log "github.com/sirupsen/logrus"
	config "github.com/spf13/viper"
)

// Define global vars for:
//
// 1). configuration file's path
// 2). log-level
// 3). wait group
var (
	conf     string
	logLevel int
	wg       sync.WaitGroup = sync.WaitGroup{}
)

// Init() is used to load the configuration file(required) and setup the log-level
//
// Only three log-levels have been supported currently
// Log-level 1: output INFO and ERROR only
// Log-level 2: output DEBUG, INFO, and ERROR
// Log-level 3: output TRACE, DEBUG, INFO, and ERROR
func init() {
	// Parse input args
	flag.StringVar(&conf, "conf", "", "")
	flag.IntVar(&logLevel, "log-level", 1, "")
	flag.Parse()

	// Set log level and logger properties
	switch logLevel {
	case 1:
		log.SetLevel(log.InfoLevel)
	case 2:
		log.SetLevel(log.DebugLevel)
	case 3:
		log.SetLevel(log.TraceLevel)
	}
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	log.Infof("Controller: Set log level to %d", logLevel)
}

// controller struct is used to:
//
// 1. control whole service which include Start, Stop, ReStart, Handle signals, support jobs, etc.,
// 2. control workers's status and behavior
type controller struct {
	input   string
	process string
	output  string
	worker  *worker.Worker
	wg      *sync.WaitGroup
	ctx     context.Context
	cancel  context.CancelFunc
}

// LoadConfFile() loads the configuration file using the -conf flag.
// ** Please note that the current configuration file only support YAML format.
func LoadConfFile() {
	// Force stop the process if configuration file wasn't found
	log.Infof("Controllor: Conf path: %s", conf)
	if conf == "" {
		log.Error("Controller: configuration is required, please specify the configuration file")
		panic("Controller: no any configuration file found")
	}

	contents, err := ioutil.ReadFile(conf)
	if err != nil {
		log.Error(err)
		panic(err)
	}

	// Force stop the process if configuration file's format is not YAML
	config.SetConfigType("yaml")
	err = config.ReadConfig(bytes.NewBuffer(contents))
	if err != nil {
		log.Error(err)
		panic(err)
	}
}

// NewController() is used to:
//
// 1). instantiate the controller
// 2). instantiate the necessary vars
// 3). return instance's pointer to caller
func NewController() *controller {
	i := config.GetString("input")
	p := config.GetString("process")
	o := config.GetString("output")

	ctx, cancel := context.WithCancel(context.Background())
	c := &controller{input: i, process: p, output: o, wg: &wg, ctx: ctx, cancel: cancel}

	return c
}

// Start() is used to:
//
// 1). Setup signal handlers
// 2). Setup helper like for metrics tracing, log backup, etc.,
// 3). Instantiate and activate workers
func (c *controller) Start() {
	// Setup signal handlers
	c.TrapSignals()

	// Init and activate workers
	c.worker = worker.NewWorker(c.input, c.process, c.output)
	c.worker.DoWork()

	// Setup helper
	c.SetHelper("Dummy job", dummy.DummyJob, "Hi, I am a dummy job", 60)
	c.SetHelper("Dump metrics job", cron.DumpMetrics, c.worker.Metrics, 5)
	c.SetHelper("Dump raw record job", cron.BackupLog, c.worker.Records, 5)
}

// ReStart() is used to:
//
// 1). Stop all tasks(goroutines) except main()
// 2). Reload the conf files
// 3). Start the  service again
func (c *controller) Restart() {
	// Stop all existing tasks
	log.Infof("Controller: restarting all tasks.")
	c.wg.Add(1)
	defer c.wg.Done()
	c.Stop()

	// Start the new tasks again after loading the new conf file
	LoadConfFile()
	c = NewController()
	c.Start()
}

// Stop() is used to:
//
// 1). Stop all tasks(goroutines) except main()
// 2). Wait until all tasks has been closed
func (c *controller) Stop() {
	input.Plugins[c.input].Stop()
	// process.Plugins[c.process].Stop()
	// output.Plugins[c.output].Stop()

	c.cancel()
	log.Infof("Controller: all tasks have been stopped.")
}

// Wait() is used to:
//
// 1). Call WaitGroup.Wait() to wait all workers until all jobs are finished
func (c *controller) Wait() {
	c.wg.Wait()
	log.Infof("Controller: all tasks has been stopped. Stopping service.")
}

// SetHelper() is used to:
//
// 1). Creat the helper for helping controller to trace metrics or backup logs, etc.,
//
// Usage: c.SetHelper(<Job Name>, <Job Func()>, <Job parameters>, <execution interval in seconds>)
func (c *controller) SetHelper(name string, helper func(interface{}), parameter interface{}, seconds int64) {
	c.wg.Add(1)
	go func() {
		log.Infof("Controller: Registered support job: %s", name)

		// Set execution interval
		d := time.Duration(seconds) * time.Second
		t := time.NewTicker(d)
		defer c.wg.Done()
		defer t.Stop()

		// Exit this function when Cancel() is being called
		for {
			select {
			case <-c.ctx.Done():
				log.Infof("Supporter: Got cancel signal. Stop support job - %s", name)
				return
			case <-t.C:
				helper(parameter)
			}
		}
	}()
}

// TrapSignals() is used to:
//
// 1). Define the process flow when receive the SIGTERM and SIGHUP
func (c *controller) TrapSignals() {
	go func() {
		// Registered Signals we defined below
		sigChan := make(chan os.Signal, 1)
		signal.Notify(sigChan, syscall.SIGHUP, syscall.SIGTERM)
		log.Info("Controller: Registered Signals: SIGTEM, SIGUSR1")

		// Start to listen on the registered signals
		for sig := range sigChan {
			switch sig {
			case syscall.SIGHUP:
				log.Info("Controller: Got SIGHUP, restarting process...")
				c.Restart()
			case syscall.SIGTERM:
				log.Info("Controller: Got SIGTERM, stopping process...")
				c.Stop()
			}
			break
		}
		log.Info("Controller: Close TrapSignals")
	}()
}
