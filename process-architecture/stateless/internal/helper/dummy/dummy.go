package dummy

import (
	log "github.com/sirupsen/logrus"
)

// The dummy function to show how to call/define the helper jobs
func DummyJob(record interface{}) {
	dummyString := record.(string)
	log.Info(dummyString)
}
