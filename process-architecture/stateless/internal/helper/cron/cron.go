package cron

import (
	"sync/atomic"
	"unsafe"

	"github.com/sirupsen/logrus"
)

func DumpMetrics(m interface{}) {
	metrics := m.(*map[string]map[string]int32)
	var oldMetrics = make(map[string]map[string]int32)
	var newMetrics = make(map[string]map[string]int32)

	newMetrics["input"] = make(map[string]int32)
	newMetrics["process"] = make(map[string]int32)
	newMetrics["output"] = make(map[string]int32)

	*(*unsafe.Pointer)(unsafe.Pointer(&oldMetrics)) = atomic.SwapPointer((*unsafe.Pointer)(unsafe.Pointer(metrics)), *(*unsafe.Pointer)(unsafe.Pointer(&newMetrics)))
	logrus.WithFields(logrus.Fields{"metrics": oldMetrics}).Info("Dump cron job event")
}

func BackupLog(r interface{}) {
	records := r.(*map[string][]string)
	var oldRecords = make(map[string][]string)
	var newRecords = make(map[string][]string)
	*(*unsafe.Pointer)(unsafe.Pointer(&oldRecords)) = atomic.SwapPointer((*unsafe.Pointer)(unsafe.Pointer(records)), *(*unsafe.Pointer)(unsafe.Pointer(&newRecords)))
	logrus.WithFields(logrus.Fields{"records": oldRecords}).Info("Dump cron job event")
}
