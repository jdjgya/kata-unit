package main

import "fmt"

type Stack struct {
	Capacity int
	Top      int
	Size     int
	MinStack []int
	Stack    []int
}

func (s *Stack) Push(element int) {

	s.Top = s.Top + 1

	if s.Top == 0 {
		s.MinStack[s.Top] = element
	} else {
		if element > s.MinStack[s.Top-1] {
			s.MinStack[s.Top] = s.MinStack[s.Top-1]
		} else {
			s.MinStack[s.Top] = element
		}
	}

	s.Stack[s.Top] = element
}

func (s *Stack) Pop() int {
	if s.Top == -1 {
		fmt.Println("No any elements can be Pop")
		return -1
	}

	popValue := s.Stack[s.Top]
	s.Top = s.Top - 1
	return popValue
}

func (s *Stack) IsEmpty() bool {
	if s.Top == -1 {
		return true
	} else {
		return false
	}
}

func (s *Stack) GetCapacity() int {
	return s.Capacity
}

func (s *Stack) GetSize() int {
	return s.Top + 1
}

func (s *Stack) GetStackMinValue() int {
	return s.MinStack[s.Top]
}

func (s *Stack) CreateStack(newCapacity int) {
	s.Stack, s.MinStack = make([]int, newCapacity), make([]int, newCapacity)
	s.Top = -1
	s.Capacity = len(s.Stack)
	s.Size = 0
}

func main() {
	s := &Stack{Capacity: 10, Top: 10, Size: 10}
	s.CreateStack(20)
	fmt.Println("New stack's size is: ", s.GetSize())
	fmt.Println("New stack's capacity is: ", s.GetCapacity())
	fmt.Println("Is new stack empty? ", s.IsEmpty())

	s.Push(3)
	s.Push(2)
	fmt.Println("Stack element: ", s.Stack)
	fmt.Println("New stack's size is: ", s.GetSize())
	fmt.Println("New stack's capacity is: ", s.GetCapacity())
	fmt.Println("Is new stack empty? ", s.IsEmpty())
	fmt.Println("Min value in stack is: ", s.GetStackMinValue())
	s.Push(1)
	fmt.Println("Min value in stack is: ", s.GetStackMinValue())

	fmt.Println("Pop element: ", s.Pop())
	fmt.Println("Min value in stack is: ", s.GetStackMinValue())
	fmt.Println("Pop element: ", s.Pop())
	fmt.Println("Pop element: ", s.Pop())
	fmt.Println("New stack's size is: ", s.GetSize())
	fmt.Println("New stack's capacity is: ", s.GetCapacity())
	fmt.Println("Is new stack empty? ", s.IsEmpty())
}
