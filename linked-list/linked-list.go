package main

import "fmt"

type LinkedList struct {
	HeadNode *LinkedNode
	LastNode *LinkedNode
}

type LinkedNode struct {
	NodeData        interface{}
	NodeAddress     *LinkedNode
	NextNodeAddress *LinkedNode
}

func (ll *LinkedList) AddFromFirst() {

}

func (ll *LinkedList) AddFromLast(data interface{}) {
	// Alwasy set data into NodeData
	node := &LinkedNode{NodeData: data}
	node.NodeAddress = node

	// Check the following thress status
	if ll.HeadNode == nil {
		// If head node's pointer is nil, then the current node must be the first node in the list
		ll.HeadNode = node
		node.NextNodeAddress = nil

	} else if ll.HeadNode != nil && ll.LastNode == nil {
		// If head node's pointer isn't nil, but last node's pointer is nil,
		// then the current node must be the second node in the list
		ll.HeadNode.NextNodeAddress = node.NodeAddress
		ll.LastNode = node.NodeAddress
		node.NextNodeAddress = nil

	} else {
		// If head node's pointer isn't nil and last node's pointer isn't nil
		// then the current node must be third ~ n node in the list.
		ll.LastNode.NextNodeAddress = node.NodeAddress
		ll.LastNode = node.NodeAddress
		node.NextNodeAddress = nil
	}
}

func (ll *LinkedList) Remove() {

}

func (ll *LinkedList) Insert() {

}

func main() {
	list := &LinkedList{}
	fmt.Println(list)

	list.AddFromLast(1)
	fmt.Printf("Node address: %p, Node data: %v, Next node: %p \n", list.HeadNode.NodeAddress, list.HeadNode.NodeData, list.HeadNode.NextNodeAddress)

	list.AddFromLast(2)
	fmt.Printf("Node address: %p, Node data: %v, Next node: %p \n", list.HeadNode.NodeAddress, list.HeadNode.NodeData, list.HeadNode.NextNodeAddress)
	fmt.Printf("Second node data: %v \n", list.HeadNode.NextNodeAddress.NodeData)

	list.AddFromLast(3)
	fmt.Printf("Third node data: %v \n", list.LastNode.NodeData)
	fmt.Printf("First node data: %v \n", list.HeadNode.NodeData)
}
