package main

import (
	"fmt"
	"runtime"
	"sync/atomic"
	"time"
	"unsafe"
)

func main() {
	// var i int32 = 0

	// go func() {
	// 	for {
	// 		i++

	// 		fmt.Printf("i = %d \n", i)
	// 		time.Sleep(1 * time.Second)
	// 	}
	// }()

	// go func() {
	// 	for {
	// 		time.Sleep(10 * time.Second)
	// 		z := atomic.SwapInt32(&i, int32(0))
	// 		i = 0
	// 		fmt.Printf("z = %d \n", z)
	// 	}
	// }()

	var m = make(map[string][]string)

	go func() {
		for {
			m["1"] = append(m["1"], "1")

			fmt.Printf("i = %s \n", m)
			time.Sleep(1 * time.Second)
		}
	}()

	go func() {
		for {
			time.Sleep(3 * time.Second)
			var old = make(map[string][]string)
			var new = make(map[string][]string)
			z := atomic.SwapPointer((*unsafe.Pointer)(unsafe.Pointer(&m)), *(*unsafe.Pointer)(unsafe.Pointer(&new)))
			_ = atomic.SwapPointer((*unsafe.Pointer)(unsafe.Pointer(&old)), z)
			fmt.Println(old)
		}
	}()

	runtime.Goexit()
}
