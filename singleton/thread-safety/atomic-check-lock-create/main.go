package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

type Singleton struct {
	Name string
}

var instance *Singleton
var mutex sync.Mutex
var instanceInited uint32

func GetSingletonInstance(name string) *Singleton {

	if atomic.LoadUint32(&instanceInited) == 0 {
		fmt.Println("Lock")

		mutex.Lock()
		defer mutex.Unlock()

		if instance == nil {
			fmt.Println("create instance")
			instance = &Singleton{Name: name}
			atomic.StoreUint32(&instanceInited, 1)
			return instance
		}
	}

	fmt.Println("Instance already created, return instance directly without locking")
	return instance
}

func main() {
	insOne := GetSingletonInstance("Bjergsen")
	fmt.Println(insOne.Name)

	insTwo := GetSingletonInstance("Cjergsen")
	fmt.Println(insTwo.Name)
}
