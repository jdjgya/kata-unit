package main

import (
	"fmt"
	"sync"
)

type Singleton struct {
	Name string
}

var instance *Singleton
var mutex sync.Mutex

func GetSingletonInstance(name string) *Singleton {

	if instance == nil {
		mutex.Lock()
		defer mutex.Unlock()
		fmt.Println("Lock")

		if instance == nil {
			fmt.Println("create instance")
			instance = &Singleton{Name: name}
			return instance
		}
	}

	fmt.Println("Instance already created, return instance directly without locking")
	return instance
}

func main() {
	insOne := GetSingletonInstance("Bjergsen")
	fmt.Println(insOne.Name)

	insTwo := GetSingletonInstance("Cjergsen")
	fmt.Println(insTwo.Name)

	insTwo.Name = "Cjergsen"
	fmt.Println(insTwo.Name)
	fmt.Println(insOne.Name)
}
