package main

import (
	"fmt"
	"sync"
)

type Singleton struct {
	Name string
}

var instance *Singleton
var mutex sync.Mutex

func GetSingletonInstance(name string) *Singleton {
	mutex.Lock()
	defer mutex.Unlock()

	if instance == nil {
		fmt.Println("create instance")
		instance = &Singleton{Name: name}
		return instance
	}
	return instance
}

func main() {
	insOne := GetSingletonInstance("Bjergsen")
	fmt.Println(insOne.Name)

	insTwo := GetSingletonInstance("Cjergsen")
	fmt.Println(insTwo.Name)
}
