package main

import (
	"fmt"
	"sync"
)

type Singleton struct {
	Name string
}

var instance *Singleton
var once sync.Once

func GetSingletonInstance(name string) *Singleton {
	once.Do(func() {
		fmt.Println("first time create instance of Singleton")
		instance = &Singleton{Name: name}
	})
	return instance
}

func main() {
	insOne := GetSingletonInstance("Bjergsen")
	fmt.Println(insOne.Name)

	insTwo := GetSingletonInstance("Cjergsen")
	fmt.Println(insTwo.Name)
}
