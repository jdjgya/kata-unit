package main

import (
	"fmt"
)

type Singleton struct {
	Name string
}

var instance *Singleton

func GetSingletonInstance() *Singleton {
	if instance == nil {
		instance = &Singleton{Name: "Bjergsen"}
		return instance
	}
	return instance
}

func main() {
	ins := GetSingletonInstance()
	fmt.Println(ins.Name)
}
