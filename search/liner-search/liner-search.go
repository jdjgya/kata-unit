package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {

	var slice []int = make([]int, 10)
	rand.Seed(time.Now().UnixNano())

	for index := range slice {
		slice[index] = rand.Intn(100)
	}

	var target int = 0
	for index := range slice {
		if target == slice[index] {
			fmt.Println("Target was found in: ", index)
			break
		}
	}
	fmt.Println(slice)
}
