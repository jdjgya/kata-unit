package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

func main() {
	// Generate the slice and assign random value to each element
	var slice []int = make([]int, 10)
	rand.Seed(time.Now().UnixNano())

	for index := range slice {
		slice[index] = rand.Intn(100)
	}

	// Sort the slice before searching
	// * The sort algo provided by Golang is Quick Sort
	sort.Ints(slice)

	// Random choose a target
	rand.Seed(time.Now().UnixNano())
	var target int = slice[rand.Intn(len(slice))]
	fmt.Println("Target is: ", target)

	// Start to search the target in given slice
	// 1. Set the upper bound and lower bound for defining the search scope
	// 2. Set mid by given upper bound and lower bound
	// 3. If mid exactly match to target, then target was found, break the loop
	// 4. If not, the compare the target and mid value.
	// 5. If mid value smaller than target, move lower bound to Mid + 1
	// 6. If mid value larger than target, move upper bound to Mid - 1
	// 7. Back to step.2 and repeat the action from 2 to 6 until Mid value equal to target
	var upperBound int = len(slice) - 1
	var lowerBound int = 0
	for true {
		mid := (upperBound + lowerBound) / 2
		if slice[mid] == target {
			fmt.Println("Target was found in #index: ", mid)
			break
		}

		if slice[mid] < target {
			lowerBound = mid + 1
		} else {
			upperBound = mid - 1
		}
	}

	foundBySearchInts := sort.SearchInts(slice, target)
	fmt.Println(foundBySearchInts)
	fmt.Println(slice)
}
