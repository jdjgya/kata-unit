package main

import (
	"fmt"
	"runtime"
	"time"
)

func ReceiveMsg(channel chan interface{}) {
	for {
		v := <-channel
		time.Sleep(5 * time.Second)
		fmt.Println("[*] Receiver: Got a message -> ", v)
	}
}

func SendMsg(channel chan interface{}) {
	for {
		channel <- "hello from SendMsg()"
		fmt.Println("[*] Sender: Hello from Sender")
	}
}

func main() {
	channel := make(chan interface{})

	// Bring up threads to capture event and parsing events
	go SendMsg(channel)
	go ReceiveMsg(channel)
	runtime.Goexit()
}
