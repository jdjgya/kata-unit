package main

import (
	"dns"
	"fmt"
)

func AssignQuery(dnsrd *dns.Dnsrecord, dnsQuery *dns.Query) {
	dnsrd.QueryField = dnsQuery
}

func main() {
	dnsQuery := &dns.Query{
		QueryClient:    "bjergsen",
		QueryDnsServer: "8.8.8.8",
		QueryDomain:    "google.com",
		QueryType:      "A",
	}
	fmt.Println(dnsQuery)

	dnsResponse := &dns.Response{}
	dnsResponse.DomainIP = "1.2.3.4"
	dnsResponse.DomainTTL = 64
	fmt.Println(dnsResponse)

	dnsrd := &dns.Dnsrecord{}
	go AssignQuery(dnsrd, dnsQuery)

	dnsrd.ResponseField = append(dnsrd.ResponseField, dnsResponse)
	fmt.Println(dnsrd)
	fmt.Println(dnsrd.GetQueryField())
}
