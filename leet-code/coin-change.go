package main

import (
	"fmt"
	"sort"
)

func coinChange(coinPool []int, amount int) {
	sort.Ints(coinPool)
	for index := len(coinPool) - 1; -1 <= index; index-- {

		if index == -1 {
			fmt.Println("Can't find the proper counts to change the $$ you gave")
			break
		}

		if amount < coinPool[index] {
			continue
		}

		var remainder int = amount % coinPool[index]
		if remainder == 0 {
			fmt.Printf("Change %d to '%d coins * %d'\n", amount, coinPool[index], amount/coinPool[index])
			break
		} else {
			fmt.Printf("Change %d to '%d coins * %d'\n", amount, coinPool[index], amount/coinPool[index])
		}
		amount = remainder
	}
}

func main() {
	var coinPool []int = []int{1, 2, 5}
	var amount int = 11
	coinChange(coinPool, amount)

	var coinPool2 []int = []int{3}
	var amount2 int = 2
	coinChange(coinPool2, amount2)
}
