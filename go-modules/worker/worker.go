package worker

import (
	"fmt"
	"time"
)

type Worker struct {
	Id   int
	Task string
}

func (wkr *Worker) DoWork() {
	for {
		fmt.Println(wkr.Task)
		time.Sleep(5 * time.Second)
	}
}
