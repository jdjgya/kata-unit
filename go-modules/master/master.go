package master

import (
	"fmt"
	"go-modules/worker"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

type Master struct {
	Task   string
	Conf   string
	Stats  string
	worker worker.Worker
}

func (m *Master) LoadConf(conf string) {
	m.Conf = conf
	fmt.Println(m.Conf)
}

func (m *Master) registerSignal() {
	fmt.Println("Register signal")
	go func() {
		fmt.Println("Start signal handler")
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGTERM, syscall.SIGUSR1)
		for sig := range c {
			fmt.Println("in sig loop...")
			switch sig {
			case syscall.SIGUSR1:
				fmt.Println("Got SIGUSR1, conf reload")
				fmt.Println(m.Conf)
			case syscall.SIGTERM:
				fmt.Println("Got SIGTERM, terminate process")
				os.Exit(0)
			}
		}
	}()
}

func (m *Master) Init() {
	m.worker = worker.Worker{Id: 1, Task: "Do something..."}
	m.registerSignal()
}

func (m *Master) Start() {
	go m.worker.DoWork()
}

func (m *Master) GetStatus() {
	fmt.Println("Current status")
}

func (m *Master) Wait() {
	wg := sync.WaitGroup{}
	wg.Add(1)
	wg.Add(1)
	fmt.Println("Start to wait for DoWork being finished")
	go m.Stop(&wg)
	wg.Wait()
	fmt.Println("after wg wait()")
}

func (m *Master) Stop(wg *sync.WaitGroup) {
	fmt.Println("try to stop the goroutine...")
	time.Sleep(5 * time.Second)
	wg.Add(-1)
	wg.Add(-1)
	fmt.Println("wg -1")
}
