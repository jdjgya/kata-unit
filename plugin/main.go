package main

import (
	"test-controller/controller"
)

func main() {
	c := controller.NewController()
	c.Start()
}
