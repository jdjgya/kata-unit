package worker

import (
	"fmt"
	"test-controller/plugins/input"
)

// type Input interface {
// 	Capture() error
// 	Parse() error
// }

// type S3DoInput struct {
// 	captureOK int64
// 	parseOK   int64
// 	forwardOK int64
// }

// func (i *S3DoInput) Capture() error {
// 	fmt.Println("I'm s3 input - capture")
// 	return nil
// }

// func (i *S3DoInput) Parse() error {
// 	fmt.Println("I'm s3 input - parse")
// 	return nil
// }

type MongoDBDoInput struct {
	captureOK int64
	parseOK   int64
	forwardOK int64
}

func (m *MongoDBDoInput) Capture() error {
	fmt.Println("I'm mongo input - capture")
	return nil
}

func (m *MongoDBDoInput) Parse() error {
	fmt.Println("I'm mongo input - parse")
	return nil
}

type worker struct {
	input map[string]input.Input
}

func NewWorker(input map[string]input.Input) *worker {
	w := &worker{input: input}
	return w
}

func (w *worker) DoWork() {
	module := "s3"
	err := w.input[module].Capture()
	err = w.input[module].Parse()

	fmt.Println(err)
	// err = w.Process()
	// err = w.Output()
}
