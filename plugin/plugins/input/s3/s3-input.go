package s3

import (
	"fmt"
	"test-controller/plugins"
)

func init() {
	s3Input := &S3DoInput{}
	plugins.Input["s3"] = s3Input
}

type S3DoInput struct {
	captureOK int64
	parseOK   int64
	forwardOK int64
}

func (i *S3DoInput) Capture() error {
	fmt.Println("I'm s3 input - capture")
	return nil
}

func (i *S3DoInput) Parse() error {
	fmt.Println("I'm s3 input - parse")
	return nil
}
