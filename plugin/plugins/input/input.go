package input

type Input interface {
	Capture() error
	Parse() error
}
