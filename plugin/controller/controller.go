package controller

import (
	"test-controller/plugins"
	_ "test-controller/register_plugins"
	"test-controller/worker"
)

type controller struct {
	NumWorker int
}

func NewController() *controller {
	var err error
	c := &controller{}
	// err = c.initController()
	if err != nil {
		panic(err)
	}

	return c
}

func (c *controller) Start() {
	w := worker.NewWorker(plugins.Input)
	w.DoWork()
}
